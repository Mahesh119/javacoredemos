package com.org.exception;
//User-Defined Checked Exception
public class EmployeeNotFoundException extends Exception{
	private int id;

	EmployeeNotFoundException(int i, String message) {
		super(message);
		this.id = i;
	}

	EmployeeNotFoundException(int i, String message, String cause) {
		super(message, new Throwable(cause));
		this.id = i;
	}

	@Override
	public String toString() {
		return String.format("EmployeeNotFoundException[%d]", this.id);
	}
}
