package com.org.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WildCardUnBounded {
	public static void display(List<?>  l)
	{
		for( Object o : l) { // Object is super class for all the the user defiend classes and all the built in classes   
			System.out.println(o);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
List<Integer> l1 = Arrays.asList(1,2,3);
System.out.println("Display the integer values ");
display(l1);
List<String> l2= Arrays.asList("A","B","C","D");
System.out.println(" displaying the string values");
display(l2);



	}

}
