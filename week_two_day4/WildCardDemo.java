package com.org.exception;

import java.util.Arrays;
import java.util.List;
// Upperbound wild card --- List<? extends Number >
class WildCard<T extends Number>// here you are bounded to use all the subclasses of Number class (Superclass)
{
	public void display()
	{
		System.out.println(" This is a bound type generics class ....");
	}
}


public class WildCardDemo {

	private static Number summation(List<? extends Number> num) {
		double sum= 0.0; // byte-> short-> int->long->float-> double
		for(Number n : num )
			sum +=n.doubleValue();
		return sum ;
	}
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Number subtype : Integer
		List<Integer> list = Arrays.asList(1,3,5,7);
		System.out.println(" Sum of the elements of Integer datatype : "+ summation(list));
		
		// Number subtype : double 
		List<Double> l1 = Arrays.asList(1.0,2.0,3.0,4.0,5.0,6.9);
		System.out.println(" Sun of the element of double datatype "+ summation(l1));
		
		//WildCard<String> t1 = new WildCard<String>(); compile time error 
		
		WildCard<Integer> w1= new WildCard();
		w1.display();
		

	}

}
