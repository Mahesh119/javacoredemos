package com.org.exception;

public class NestedTryDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 try {
	 //parent try block 
	  System.out.println(" Parent Try block");
	  
	  try {
		   //child try block 1 
		  System.out.println(" Inside block 1 ");
		  int b = 10/0;
		  System.out.println(b);
	  }catch (ArithmeticException e) {
		// TODO: handle exception
		  System.out.println("Exception :1"+ e);
	}
	  try {
		  //child try block 2
		  System.out.println(" Inside block 2");
		  int x []= new int [5];
		 x[10]=10;
	  }catch (NullPointerException e) {
		// TODO: handle exception
		  System.out.println("Exception : 2"+ e);
	}
	   
 }catch (ArrayIndexOutOfBoundsException e) {
	// TODO: handle exception
	 System.out.println(" Array out of bound index");
	 System.out.println("Inside parent try block ");
}
 catch (Exception e) {
	// TODO: handle exception
	 System.out.println("Exception");
	 System.out.println(" Inside Parent try catch block ");
}
 System.out.println(" Next statement ....");
	}

}
