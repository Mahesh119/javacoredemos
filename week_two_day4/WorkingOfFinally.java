package com.org.exception;
//Contains code that must be executed no matter if an exception is thrown or not.
//It contains code of file release, closing connections, etc.
public class WorkingOfFinally {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			//int res = 20/0 ; //exception in code 
			
			int res= 25/5; // no exception in code 
			int x[] = new int[5];
			x[6]=12; //exception is here but not handled by the catch block 
			System.out.println("Result "+res);
		}catch (ArithmeticException e) {
			// TODO: handle exception
			System.out.println(" handle the exception ");
			System.out.println(e);
		}
		//finally block execute regardless of exception or not
		
		finally {
			System.out.println("1) Finally block executing when exception occuring ....");
			System.out.println("2) finally block executing when exception not occuring....");
			System.out.println("3) finally block executing when an exception occure but not handled by the catch block");
		}
		
	}

}
