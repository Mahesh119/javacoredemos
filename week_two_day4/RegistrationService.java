package com.org.exception;

import java.util.Arrays;
import java.util.List;

public class RegistrationService {

	List<String> email = Arrays.asList("abc@gmail.com" , "xyz@gmail.com","dfg@yahoomail.com" ,"pqr@gmail.com");
	
	public void validateEmail(String e) throws EmailNotUniqueException {
		if(email.contains(e)) {
			throw new EmailNotUniqueException("Email Already registered" );
		}
	}
}
