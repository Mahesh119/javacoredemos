package com.org.exception;

//Generic class 
class MyGenericClass<T>
{
	T obj; //type object 
	void add(T obj) {
		this.obj=obj;
	}
 T get(){
		return  obj;
	}
}

public class GenericDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
     MyGenericClass<Integer> m1 = new MyGenericClass<Integer>();
     m1.add(24);
     
     MyGenericClass<String> m2 = new MyGenericClass<String>();
     m2.add("Monday");
     
     System.out.println(" MyGenericClass<Integer> "+ m1.get());
     System.out.println("MyGenericClass<String> "+m2.get());
     
	}

}
