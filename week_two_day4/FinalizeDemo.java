package com.org.exception;

public class FinalizeDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
     FinalizeDemo obj = new FinalizeDemo();
     obj = null ;
     System.gc();
     System.out.println(" done");
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
		System.out.println(" Before destroy the object perform some activity");
		System.out.println(" release connection");
		System.out.println(" Close network connection");
	}
	

}
