package com.org.hcl;

public class UseOfStatic {
String emp_name ; int emp_id ;
static String company_name ="HCL" ;
static {//static block
	System.out.println(" Loding before main method");
}

static void save_data() {//static method
	System.out.println(" Saving memry space by using static methods");
}

void set_data(String name , int id ) {
	emp_name=name;
	emp_id=id;
}
void com_data() {
	int foundation_date =123;
	int total_emp =40000;
	System.out.println(" Company Name  "+company_name +" Foundation date " + foundation_date+" Total emp "+ total_emp);
}
void printdata() {
	System.out.println("Name :"+emp_name + " Emp_ id "+ emp_id +" working in  "+ company_name);// accessing static variable
}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
     UseOfStatic.company_name="HCL_PRO";// changing the static variable value 
		UseOfStatic u1 = new UseOfStatic();
		UseOfStatic u2 = new UseOfStatic();
		UseOfStatic u3 = new UseOfStatic();
		u1.set_data("Pardeep" , 111);u2.set_data("Rani", 234);u3.set_data("Ramesh", 333);
		u1.printdata(); u2.printdata();u3.printdata();
		u1.com_data();
		UseOfStatic.save_data();//calling static method
	}

}
