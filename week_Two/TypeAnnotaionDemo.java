package com.org.collection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE_USE)
//declaring a simple type annotaion
@interface TypeAnnotaion{}

public class TypeAnnotaionDemo {
	
	static @TypeAnnotaion int method(int x) {
		System.out.println(" Static method of the calss");
		return x;
	}
    public  @TypeAnnotaion  float m2(float f) {
    	System.out.println(" flaot data ");
    	return f;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
  @TypeAnnotaion  String Address = " I am the type annotaion example ";
	
	System.out.println(Address);
	method(100);
	
	}
	

}
