package com.org.collection;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

@Retention(RetentionPolicy.RUNTIME)
/* full annotation
@Entity(tableName = "Car" , primarykey = "carId")//full annotation
@InsertNew(value="yes")*/
@interface Demo{
	int value(); //single valued 
	int m1() default 0;
}


//--------------------------------- single value Annotation-----------------

public class SingaleValueAnnotation {

	@Demo(220)
	public static void MyDemo() throws NoSuchMethodException, SecurityException {
		SingaleValueAnnotation s1= new SingaleValueAnnotation();//class object
		 Method m = s1.getClass().getMethod("MyDemo");//
	   
	     Demo d1 = m.getAnnotation(Demo.class);
	     System.out.println(d1.value());
	}
	// SingaleValueAnnotation s1 = new SingaleValueAnnotation();
     
    
	public static void main(String[] args) throws NoSuchMethodException, SecurityException {
		// TODO Auto-generated method stub
		MyDemo(); // calling method
     
      
      
	}

}
