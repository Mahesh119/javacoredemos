package com.org.collection;

// create Novel class ( Bid , NovelName  , Novel_price , writer ) and create object into BookCollection 

public class Book {
int bookid;
String bookName ;
String author;
String publisher;
int quantity;
public Book(int bookid, String bookName, String author, String publisher, int quantity) {
	super();
	this.bookid = bookid;
	this.bookName = bookName;
	this.author = author;
	this.publisher = publisher;
	this.quantity = quantity;
}

}
