package com.org.collection;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 ArrayList<String> obj = new ArrayList<String>();
 ArrayList<String> obj1 = new ArrayList<String>();
 ArrayList< Integer> obj2 = new ArrayList<Integer>();
 obj.add("Apoorva");obj.add("Ajeet");obj.add("Shubham");obj.add("Bharti");// adding into arraylist
 obj.add(2, "Nishi");
 obj1.add("Suneel"); obj1.add(" Shree");obj1.add("Nitin");obj1.add("sunitha");obj1.add("pradeep");obj1.add("Lata");
 //append collection element
 
 System.out.println(" Element Added :" + obj);
 System.out.println("Obj1 "+obj1);
 obj.addAll(obj1); // 
 System.out.println(" simple append :" +obj);
 obj.addAll(3, obj1);
 System.out.println(" At the Index : "+obj);

 // remove element 
 obj.remove(8);obj.remove("Shree");
 System.out.println(" After removing element : "+obj);
 
 //sublist 

 System.out.println(" original list "+ obj);
 
 ArrayList<String > obj3 = new ArrayList<String>(obj.subList(2, 4));
 System.out.println(" Sublist stored inArraylist :"+ obj3);
 
 // check the element is available or not 
 
 System.out.println(" Shree is available ? : "+ obj.contains("Suneel"));
 
 //clone --- shallow copy 
 
 ArrayList< String> copy= (ArrayList<String>)obj.clone();
 System.out.println("clone copy :"+copy);
 
 //sorting arraylist 
 System.out.println(" Before sorting "+ obj);
 Collections.sort(obj);
 System.out.println(" after sorting "+ obj);
	}

}
