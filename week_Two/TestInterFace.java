package com.org.collection;

public class TestInterFace implements PrintData ,DemoInterface{ // replacing multiple inheritance by interface{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      TestInterFace t1= new TestInterFace();
      t1.print();t1.getData();t1.ShowData();//calling abstract methods
      t1.input();//calling default method
      PrintData.HaveData();//calling static method by interface name
      
      // java 9 private methods accessing 
      
  /*    PrintData p = new TestInterFace();
      p.sayJava9();*/
      
      System.out.println(" My Account Number is : "+ t1.Account_no);
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		System.out.println(" Print Data ");
	}

	@Override
	public void getData() {
		// TODO Auto-generated method stub
		System.out.println(" Get Some Data from class");
	}

	@Override
	public void ShowData() {
		// TODO Auto-generated method stub
		System.out.println(" Show some Data from the class ");
	}
	public void demo() {
		System.out.println(" Demo data");

}
}