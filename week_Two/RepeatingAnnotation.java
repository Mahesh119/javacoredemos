package com.org.collection;

import java.lang.annotation.Annotation;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

// f1 annotation 
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(f2.class)
@interface f1{
String f1() default " Repeating Annotaion" ;
int value() default 0;
}

//create container annotation
@Retention(RetentionPolicy.RUNTIME)
@interface f2{ //container specified as an annotation whose value field is an array of the repeatable annotation type
	f1[] value();
}


//-------------------------------------------
public class RepeatingAnnotation {

	@f1(f1= "java" , value = 1)
	@f1(f1="Advance Java" , value = 2)
	public static void MyMethod() throws NoSuchMethodException, SecurityException {
		RepeatingAnnotation r1 = new RepeatingAnnotation();
		 
		Class<?> c= r1.getClass();
		
		Method m = c.getMethod("MyMethod");
		
		// displaying repeated annotation
		Annotation anno = m.getAnnotation(f2.class);
		System.out.println(anno);
	}
	
	public static void main(String[] args) throws NoSuchMethodException, SecurityException {
		// TODO Auto-generated method stub
     MyMethod();
	}

}
