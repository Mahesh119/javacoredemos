package com.org.collection;

import java.util.ArrayList;

public class BookCollection {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    ArrayList<Book> bookdata = new ArrayList<Book>();
    
    //create book data
    Book b1 = new Book(101, " Java ", "ABC", "BPB", 10);
    Book b2= new Book(102, "Spring ", "abb", "BPB", 20);
    Book b3 = new Book(103, "Operating system", "Galvin", "wily", 15);
    Book b4 = new Book(104, "Let Us C", "YC", "BPB", 12);
    
    bookdata.add(b1);bookdata.add(b2);bookdata.add(b3);bookdata.add(b4);
    
    for(Book b : bookdata)
    	System.out.println("Book Id :"+ b.bookid + "    BookName  : "+ b.bookName +" Auther :"+ b.author+" Publisher : "+ b.publisher);
    
	}

}
