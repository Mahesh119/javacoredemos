package com.org.collection;

public interface PrintData {
	 int Account_no = 100;//public static final
	
void print();// public and abstract method

void getData();//abstract method
void demo();
void ShowData();//abstract method-- prior version of java 

//-------------------------- Java 8 feature -------------------------------------------
//default method
default void input() {
	//non - abstract method
	System.out.println(" This is default method of Java version 8 ");
}
default void print2D() {
	System.out.println(" This is java 8 default method which is  printing  2D.....");
}
default void demo1() {
	System.out.println("This is Printdata demo1()");
}

// Static method

static void HaveData() {
	//non abstract
	System.out.println(" This is Java 8 Static Method  ....");
	
}

static void m1() {
	System.out.println(" Java 8 static method in interface ");
}

//------------------------ Java 9 feature --------------------------------------------------------

//private method
/*
private void sayJava9() {
	System.out.println(" This is Java 9 private method in interface");
}
*/

}
