package com.org.multiT1;

public class MultipleThreadDemo extends Thread {

	String task;
	
	
	public MultipleThreadDemo(String task) {
		super();
		this.task = task;
	}
public void run() {
	for(int i =1;i<=5;i++){
		System.out.println( task+ " : Task " +i);
		try {
			Thread.sleep(1000);
		}catch (InterruptedException e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
	}
}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
   MultipleThreadDemo m1 = new MultipleThreadDemo("Cut The Ticket ");//passing task as an argument to its constructor 
   
   MultipleThreadDemo m2 = new MultipleThreadDemo(" Show your seat Number ");
   
   // create two objects of thread class and pass two objects as parameter to constutor of thread class.
 // m1.start();m2.start();
   
  Thread t1 = new Thread(m1);
   Thread t2 = new Thread(m2);
   
   t1.start();//  is executed by JVM, it starts execution of code inside run()
   //method and print the statement �Cut the ticket� on the console.
   t2.start(); 
	}

}
