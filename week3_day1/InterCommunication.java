package com.org.multiT1;
class A{
	int i ;
	boolean flag= false ;//flag will be true when data production is over 
	synchronized void deliver(int i) {
		if(flag)
			try {
				wait();//wait till a notification is received from thread 22 there will be no wastage of time
			}catch (Exception e) {
				// TODO: handle exception
			}
		this.i=i;
		flag = true;//when data production is over it will store true into flag
		System.out.println(" data Delivered : "+ i);
		notify();//when data prod is over , it will notify thread22 to use it.
	}
	synchronized int receive() {
		if(!flag)
			try {
				wait();//wait till notification is received from thread11
			}catch (Exception e) {
				// TODO: handle exception
			}
		System.out.println(" Data received : "+ i);
		flag=false;
		notify();
		return i;
	}
}
class Thread11 extends Thread{
	A obj;// declare obj / class type variable
	
	public Thread11(A obj) {
		super();
		this.obj = obj;
	}

	public void run() {
		for(int j=1;j<=5;j++) {
			obj.deliver(j);
		}
	}
}
class Thread22 extends Thread{
	A obj;

	public Thread22(A obj) {
		super();
		this.obj = obj;
	}
	public void run() {
		for(int k=0;k<=5;k++) {
			obj.receive();
		}
	}
}
public class InterCommunication {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
A obj = new A();
Thread11 t1= new Thread11(obj);
Thread22 t2 = new Thread22(obj);
t1.start();t2.start();
	}

}
