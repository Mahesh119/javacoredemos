package com.org.multiT1;
/* Multitasking 
 * process based Multitasking (Mutliprocessing )
 * Thread based Multitasking (Multithreading)
 * thread can create in two way 
 * 1) By Extending Thread Class -- Thread() , Thread( String name ) , Thread ( Runnable r ) , 
 * Thread ( Runnable r , String name ) -- run() , start() , join() , getpriority() , getname() , setpriority(), sleep() 
 * 2) By Implementing Runnable interface 
 * */
public class ThreadDemo extends Thread {

	public void run() { // T1 is waiting for the run method and now thread is running 
		System.out.println(" Thread is in running state ......");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
     ThreadDemo T1 = new ThreadDemo(); // new born thread 
     ThreadDemo T2 = new ThreadDemo(); //new born thread
		
       T1.start();// putting thread into runnable 
       T2.start(); 
		
	}

}
