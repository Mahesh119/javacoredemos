package com.org.multiT1;
class Table {
	synchronized void printTable(int n) {
		for(int i = 1 ; i<=5;i++) {
			System.out.println(n*i);
			try {
				Thread.sleep(1000);
			}catch (Exception e) {
				System.out.println(e);
			}
		}
	}
}
class Thread1 extends Thread
{
	Table t ;//class type variable 	Table t 
	public Thread1(Table t) {
		super();
		this.t = t;
	}
	public void run() {
		//System.out.println(" printing table of 5 ....");
		t.printTable(5);
	}
}
class Thread2 extends Thread{
	Table t;

	public Thread2(Table t) {
		super();
		this.t = t;
	}
	public void run() {
		//System.out.println(" printing table of 10....");
		t.printTable(10);
	}
}

public class SynchronizedDemo1 extends Thread {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Table obj = new Table();
Thread1 t1= new Thread1(obj);
Thread2 t2 = new Thread2(obj);
t1.start();t2.start();
	}

}
