package com.org.multiT1;

public class SleepThread1 extends Thread {
//run method for thread
	public void run() {
		for(int i= 1 ; i<=5 ; i++) {
			try {
				//call sleep method of thread
				//whenever thread obj comes under the run methods current thread will sleep for the milis 
				Thread.sleep(1000);
			}catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
			//print current thread instance with loop variable
			System.out.println(Thread.currentThread().getName()+"  :  "+ i );
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
     SleepThread1 t1= new SleepThread1();
     SleepThread1 t2 = new SleepThread1();
     // start thread one by one 
     t1.start();// thread 0 
     t2.start();// thread 1 
	}

}
