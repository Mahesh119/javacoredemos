package com.org.multiT1;

public class SleepThread3 implements Runnable {

	@Override
	public void run() {
		// TODO Auto-generated method stub
     System.out.println(" Thread running "+ Thread.currentThread());
     try {
    	 Thread.sleep(2000);
     }catch (InterruptedException e) {
		// TODO: handle exception
    	 e.printStackTrace();
	}
     System.out.println(" hello Java Sleep method");
     System.out.println(Thread.currentThread());
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
     SleepThread3 t1 = new SleepThread3(); // main thread
     Thread t= new Thread(t1, "child Thread"); // child thread
     
     t.start();
     t.interrupt();
     
	}

}
