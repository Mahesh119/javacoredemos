package com.org.jdk81;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

//Consumer interface - It represents a function which takes in one argument and produces a result. 
//However these kind of functions don�t return any value.

//it consists two function - accept() -method accepts one value and performs the operation on the given argument
//, andThen()-It returns a composed Consumer wherein the parameterized Consumer will be executed after the first one

public class ConsumerInterfaceDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
  //consumer to display number 
		Consumer<Integer> disp = a->System.out.println(a); 
		
		//implement disp using accept()
		disp.accept(100);
		
		//consumer to multiply 2 to every integer of a list 
		 Consumer<List<Integer> > modify = list ->
	        {
	            for (int i = 0; i < list.size(); i++)
	                list.set(i, 2 * list.get(i));
	        };
	        // Consumer to display a list of numbers
	        Consumer<List<Integer> >
	            dispList = list -> list.stream().forEach(a -> System.out.print(a + " "));
	 
	        List<Integer> list = new ArrayList<Integer>();
	        list.add(2);
	        list.add(1);
	        list.add(3);
	        // Implement modify using accept()
	        modify.accept(list);
	 
	        // Implement dispList using accept()
	        dispList.accept(list);
	}

}
