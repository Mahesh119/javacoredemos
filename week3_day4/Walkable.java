package com.org.jdk81;

public interface Walkable {
default void move() {
	System.out.println(" This is walkable interface move()");
	System.out.println(" Walkable interface default method");
}
public void walk();

}
