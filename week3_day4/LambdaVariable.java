package com.org.jdk81;

import java.util.Locale.Category;
/*Lambda rules 
 * 1) parameter are optional it can be zero or more 
 * ()->{system.out.print("hello");
 * (int a) ->{system.out.print("value=" + a*a);
 * 
 * 2) if not parameter are available then we need to use empty parenthesis()
 * ()->{sysout};
 * 
 * 3) if we have parameter then need to separate with comma
 * (int x , int y )->x*y
 * 4) if the body has only statement then curly braces are optional 
 * (int a) ->System.out.print("value="+ a);
 * 5) we can assign lambda to functional interface
 * funtional interface obj = (inta , int b )->(a+b);
 * */
public class LambdaVariable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
    BookInterface book=(Category)->{
    	int localLambdaVar = 100; //local variable to lambda can not access from out side of it 
    	localLambdaVar++;
		return localLambdaVar;
		
    	
    };
     int count = book.getBooksCount("Technology");
     System.out.println("Count "+count);
	}

}
