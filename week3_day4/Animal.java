package com.org.jdk81;

import java.util.ArrayList;
import java.util.List;

public class Animal implements Moveable ,Walkable{

	public void eat() {
		System.out.println(" Tiger Like Meat !!!!");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//can call default method of interface in this way also
   Animal tiger = new Animal();
   tiger.move();
   tiger.eat();
   //Walkable.super.move();
   //Moveable.super.move();
   
   
	}
	@Override
	public void walk() {
		// TODO Auto-generated method stub
		System.out.println(" This is walkable interface singal abstract method ");
	}
	@Override
	public void move() {
		// TODO Auto-generated method stub
		//Walkable.super.move();
		Moveable.super.move();
	}

}
