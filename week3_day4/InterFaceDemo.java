package com.org.jdk81;
//only one abstract method is allowed in any functional interface.
//Second abstract method is not not permitted in a functional interface.

/*function interface is valid even if @functionalinterface annotation would be omitted. it is only for informing
it is only for informing the compiler to enforce single abstract method inside interface 

*it can contain default , static methods 
*an interface declares an abstract method overriding one of the public methods of java.lang.Object,
* that also does not count toward the interface�s abstract method count
*/
@FunctionalInterface
public interface InterFaceDemo {
public void m1();//abstract method 

default void show() {//default method
	System.out.println(" default");
}
//default method
default void print() {
	System.out.println(" Print ");
}
static void  getData() {//static  method
	System.out.println(" Get Data");
}
@Override
boolean equals(Object obj);

@Override
String toString();




}
