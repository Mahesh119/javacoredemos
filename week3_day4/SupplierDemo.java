package com.org.jdk81;

import java.util.ArrayList;
import java.util.function.Supplier;

// It represents a function which does not take in any argument but produces a value of type T.
//Suppliers are useful when we don�t need to supply any value and obtain a result at the same time.//it has get method
public class SupplierDemo {

	private static void printName(Supplier<String> supp) {
		System.out.println(supp.get());
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
 System.out.println(" supplier demo ---");
 ArrayList<String > name = new ArrayList<String>();
 name.add("Mithuna");
 name.add("Bhagyashree");
 name.add("Akshadeep");
 name.add("Nitin");
 
 name.stream().forEach((item)-> {printName(()-> item);});
}
	}


