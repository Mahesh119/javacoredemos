package com.org.generic;

import java.util.HashMap;

public class HashMapEx4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
HashMap<Character, String> h1 = new HashMap<Character, String>();
h1.put('R', "Red");h1.put('O', "Oreange");h1.put('W', "White");h1.put('B', "Blue");

//display 
System.out.println(" Entries in HashMap : "+ h1);

//Replace value for specified key 
Object replaceValue = h1.replace('W', "White_Snow");
System.out.println(" Replaced Vlaue : "+ replaceValue);
System.out.println(" Updated map "+ h1);

boolean replaceValue2 = h1.replace('R', "Red", "Hot Red");
System.out.println(" Replaced Vlaue : "+ replaceValue2);
System.out.println(" Updated map "+ h1);

//serach opeartion 
String s = h1.get('R');
System.out.println(" R for what ? "+ s);

String s1 = h1.getOrDefault('G', "Green");
System.out.println(" G for what ? "+ s1);


	}

}
