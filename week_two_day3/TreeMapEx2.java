package com.org.generic;

import java.util.TreeMap;

public class TreeMapEx2 {
	public static void main(String[] args) 
	{
	TreeMap<Character, String> tmap = new TreeMap<>();
	  
	  tmap.put('A', "Apple");
	  tmap.put('P', "Parot");
	  tmap.put('C', "Cat");
	  tmap.put('B', "Boy");
	  tmap.put('D', "Dog");
	 
	Object entrySet = tmap.entrySet();
	System.out.println("Entry set: " +entrySet);
	System.out.println("Key set: " +tmap.keySet());
	System.out.println("Value set: " +tmap.values());

	Object vGet = tmap.get('C');
	System.out.println("C: " +vGet);

	boolean containsKey = tmap.containsKey('B');
	System.out.println("Is key 'B' present in map: " +containsKey);

	boolean containsValue = tmap.containsValue("Apple");
	System.out.println("Is Apple present in map: " +containsValue);
	
	System.out.println("ceilingEntry "+ tmap.ceilingEntry('B'));
	System.out.println("ceilingKey "+tmap.ceilingKey('B'));
	
	System.out.println("FirstEntry "+tmap.firstEntry());
	System.out.println("LastEntry "+tmap.lastEntry());
	
	System.out.println("FloorEntry "+tmap.floorEntry('B'));
	System.out.println("HigherEntry "+tmap.higherEntry('P'));
	System.out.println("LowerEntry "+tmap.lowerEntry('D'));
	
	 }
}
