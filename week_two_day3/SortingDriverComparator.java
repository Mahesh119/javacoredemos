package com.org.generic;

import java.util.ArrayList;
import java.util.Collections;

public class SortingDriverComparator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
ArrayList<Auther> al = new ArrayList<Auther>();

al.add(new Auther("Henray", "Young girl Book", 45));
al.add(new Auther("Nalo", "Brown Girl in the Ring", 61));
al.add(new Auther("Chetan Bhagat", "2 States", 50));
al.add(new Auther("Frank", "300", 62));
al.add(new Auther("Deborah", "Four Boys", 51));

System.out.println(" sorting by author First Name : ");
Collections.sort(al);
for(Auther a : al) {
	System.out.println(a.getFirstName()+"  , "+a.getBookName()+" , "+a.getAuAge());
}
// sorting author data by age 
System.out.println(" sorting by Author Age : ");
Collections.sort(al, new AuthorAgeComparator());
for(Auther a1 : al) {
	System.out.println(a1.getFirstName()+"  , "+a1.getBookName()+" , "+a1.getAuAge());
	}
System.out.println("sorting by Book name : ");
Collections.sort(al, new BookNameComparator());
for(Auther a1 : al) {
	System.out.println(a1.getFirstName()+"  , "+a1.getBookName()+" , "+a1.getAuAge());
	}

}
}