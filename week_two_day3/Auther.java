package com.org.generic;

public class Auther implements Comparable<Auther> {
String firstName; String bookName; int auAge ;

	public Auther(String firstName, String bookName, int auAge) {
	super();
	this.firstName = firstName;
	this.bookName = bookName;
	this.auAge = auAge;
}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public int getAuAge() {
		return auAge;
	}

	public void setAuAge(int auAge) {
		this.auAge = auAge;
	}

	@Override
	public int compareTo(Auther ar) {
		// TODO Auto-generated method stub
	//	System.out.println(this.firstName);
	//	System.out.println(ar.firstName);
		return this.firstName.compareTo(ar.firstName);
	}

}
