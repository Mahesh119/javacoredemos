package com.org.generic;

import java.util.HashMap;
import java.util.TreeMap;

//it contains only unique elements , it can not have null key value , 
//it maintains ascending order 
// iterator returned by treemap is fail fast means we cannot modify map during iteration.
public class TreeMapEx1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 HashMap<String , String> h1 = new HashMap<String, String>();
 
 h1.put("R", "Red");h1.put("O", "Orange");h1.put("P", "Pink");h1.put("G", "Green");
 
 System.out.println(" Entries of HashMap : "+h1);
 
 TreeMap<String, String> t1= new TreeMap<String, String>(h1);
 System.out.println(" Treemap entries :"+ t1);
 System.out.println(" Entries in ascending order : "+ t1);
 
 System.out.println("Remove entries :"+ t1.remove("O"));
String s1 = t1.replace("P", "Purple"); 
 System.out.println(" Replace P with purple  "+ s1);
 System.out.println(t1);
	}

}
