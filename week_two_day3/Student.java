package com.org.generic;

public class Student {
String name ; 
int age;//sort
int fees;//sort 
int marks;//sort
public Student(String name, int age, int fees, int marks) {
	super();
	this.name = name;
	this.age = age;
	this.fees = fees;
	this.marks = marks;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public int getFees() {
	return fees;
}
public void setFees(int fees) {
	this.fees = fees;
}
public int getMarks() {
	return marks;
}
public void setMarks(int marks) {
	this.marks = marks;
}

}
