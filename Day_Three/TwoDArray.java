package com.org.day3;

import java.util.Scanner;

public class TwoDArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] a = {
	            {1, 2, 3}, //a[0] --row1
	            {4, 5, 6, 9}, //a[1]--row2
	            {7}, //a[2]---row3 
	        };// style 1 
		  // calculate the length of each row
        System.out.println("Length of row 1: " + a[0].length);
        System.out.println("Length of row 2: " + a[1].length);
        System.out.println("Length of row 3: " + a[2].length);
        
	      int [][] b= new int[3][4];//style 2
	      Scanner sc = new Scanner(System.in);
	      System.out.println(" Please enetr the value to be added");
	      for (int i =0 ; i<3;i++) {
	    	  for(int j =0 ;j<4 ; j++) {
	    		  b[i][j]=sc.nextInt();
	    	  }
	    	  System.out.println();
	      }System.out.println(" Your output");
	      
	     
	for(int i=0;i<3;i++) {
		for(int j =0;j<4;j++) {
			System.out.print(b[i][j]+ "    ");
	      }
	      System.out.println();
	}

	}}
