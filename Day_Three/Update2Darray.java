package com.org.day3;

public class Update2Darray {

	static void printArray(int [][] arr) {
		for(int i=0 ;i<arr.length ;i++) {
			for(int j =0;j<arr[i].length;j++) {
				System.out.print(arr[i][j]);
			}
			System.out.println();
		}
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
int [] [] arr= {{10,20},{21,22},{100,140}};
System.out.println("before updating an array");
printArray(arr); //calling method

arr[2][0]=45; arr[2][1]=50;
System.out.println(" After updating an array");
printArray(arr);






	}

}
