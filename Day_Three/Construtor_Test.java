package com.org.day3;
//default constructor
//default constructor is used to provide the default values to the object like 0, null, etc
public class Construtor_Test {
int classId ; 
String  className;
float classParam;

public Construtor_Test() {//default constructor
	super();
	// TODO Auto-generated constructor stub
}

void display() {
	System.out.println(" Class Id : "+classId+ "\n Class Name : "+className+ "\n Class Parameter : "+ classParam);
}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//creating object 
Construtor_Test c1 = new Construtor_Test();//default constructor created by compiler
Construtor_Test c2 = new Construtor_Test();

c1.display();
c2.display();
	}

}
