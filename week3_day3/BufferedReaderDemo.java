package com.org.jdk8;
//character stream - Reader -- bufferedreader , File reader  , CharArrayReader , InputstreamReader

//bufferedReader extends reader 
import java.io.BufferedReader;
import java.io.FileReader;
public class BufferedReaderDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
char [] array = new char[100];
try {
	//create file reader
	FileReader file = new FileReader("D:\\read\\reader.txt");
	
	//create bufferreader 
	BufferedReader buffer = new BufferedReader(file); //will store the file or data into the buffer area so disk space not need to access multiple time 
	
	//read characters 
	buffer.read(array);
	System.out.println(" data in the file :");
	System.out.println(array);
	
	//close
	buffer.close();
	
}catch (Exception e) {
	// TODO: handle exception
}
		
	}

}
