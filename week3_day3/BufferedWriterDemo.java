package com.org.jdk8;
//bufferedWriter extends Writer
//question - write a program to read from a file using Bufferedreader class
import java.io.BufferedWriter;
import java.io.FileWriter;

//bufferedWriter maintains as internal buffer of 8192 character 
// during the write operation char are written to the internal buffer instead of the disk and ones the disk is filled the whole 
// char in the buffer are written to the disk
public class BufferedWriterDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
  String data = " hello Java" ;
  String data2 = " My file ";
  
  try {
	  FileWriter file = new FileWriter("D:\\read\\reader.txt");
	 
	  BufferedWriter out = new BufferedWriter(file);
	  
	  out.write(data);out.write(data2);
	  
	  //flushes data to the destination
	  out.flush();
	  System.out.println("data is Flushes to the file ");
	  System.out.println("success");
	  
	  out.close();
	  
  }catch (Exception e) {
	// TODO: handle exception
}
	}

}
