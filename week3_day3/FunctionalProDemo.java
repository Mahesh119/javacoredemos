package com.org.jdk8;

public class FunctionalProDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/* java 7 
      Runnable r = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println(" Running in runnable thread ");
		}
	};
	r.run();
	System.out.println(" Main thread ");
	}*/
	
	// java 8 
	
	Runnable r =()->System.out.println("Running in Runnable thread ");
	r.run();
	System.out.println("Running in main method ");

}
}
