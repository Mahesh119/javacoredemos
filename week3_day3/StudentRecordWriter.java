package com.org.jdk8;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class StudentRecordWriter {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		
		if(args.length<1) {
			System.out.println(" Please provide output file ");
			System.exit(0);
		}
		String outFile = args[0];
DateFormat date = new SimpleDateFormat("MM-dd-yyyy");
		
		try {
			ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream("D:\\read1.txt"));
			
			ArrayList<Student> stu= new ArrayList<Student>();
			stu.add(new Student("Amit", date.parse("02-10-2002"), false, 23, 80.5f));
			stu.add(new Student("Rohit", date.parse("06-15-2012"), false, 25, 88.5f));
			stu.add(new Student("Ravi", date.parse("08-22-2000"), false, 23, 80.5f));
			stu.add(new Student("Vijay", date.parse("02-09-2002"), false, 23, 80.5f));
			
			for(Student s : stu) {
				obj.writeObject(stu);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
