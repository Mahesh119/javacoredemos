package com.org.jdk8;

import java.io.Serializable;
import java.util.Date;

public class Student implements Serializable {
private String name ;
//private Date bithday;
private boolean gender;
private int age;
private float gread;
public Student() {
	super();
	// TODO Auto-generated constructor stub
}
public Student(String name, boolean gender, int age, float gread) {
	super();
	this.name = name;
	//this.bithday = bithday;
	this.gender = gender;
	this.age = age;
	this.gread = gread;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

public boolean isGender() {
	return gender;
}
public void setGender(boolean gender) {
	this.gender = gender;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public float getGread() {
	return gread;
}
public void setGread(float gread) {
	this.gread = gread;
}


}
