package com.org.coll2;

import java.util.Stack;

public class StackDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Stack<Integer> even = new Stack<Integer>();
even.push(23);even.push(45);even.push(55);even.push(88);

System.out.println(" Initial stack "+ even);
//search element 
int loc = even.search(45);
System.out.println(" search value :"+ loc);

loc = even.size();
System.out.println(" size  " +loc);
//pop 
System.out.println(even.pop());
System.out.println(even.pop());
System.out.println(" Stack after pop operation "+even);
	}

}
