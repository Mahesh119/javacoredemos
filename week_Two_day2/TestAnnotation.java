package com.org.coll2;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


//user defined /custom annotation 
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface TestAnnotation {

	String Tester() default "Abhay" ;
	String TestCase();// this will retained at runtime 
}
