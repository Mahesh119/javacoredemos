package com.org.coll2;

public class DriverClassEqualMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s1 = "ABC" ; String s2= "ABC";
		if(s1.equals(s2)) {
			System.out.println( s1.hashCode() + "    "+s2.hashCode());
		}
		
		
		
     CustomerData obj1 = new CustomerData( " ABC" , 101);
     CustomerData obj2 = new CustomerData("xyz", 102);
    // CustomerData obj2 = new CustomerData(obj1);
     
     if(obj1.hashCode()==obj2.hashCode()) {
    	 if(obj1.equals(obj2))
    		 System.out.println(" Both are equal");
    	 else 
    		 System.out.println(" not equal");
     }
     else 
    	 System.out.println(" not equal");
	}

}
