package com.org.coll2;

import java.util.HashSet;

public class SetOperation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
HashSet<Integer> h1 = new HashSet<Integer>();
h1.add(2);h1.add(4);h1.add(5);
System.out.println("Hashset 1 "+ h1);

HashSet<Integer> h2 = new HashSet<Integer>();
h2.add(8);
h2.add(10);h2.add(5);
System.out.println(" Hashset 2 "+h2);

//union operation 
h1.addAll(h2);
System.out.println(" Union Opearion : "+ h1);

//intersection operation 
h1.retainAll(h2);
System.out.println(" Intersection operation : "+ h1);

//difference of the sets 
h2.removeAll(h1);
System.out.println(" Difference of sets : "+ h1);

//subset 
boolean result = h1.containsAll(h2);
System.out.println(" Is H2 is the subset of H1 ? "+ result);

	}

}
