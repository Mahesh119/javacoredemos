package com.org.coll2;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented //used to indicate
@Retention(RUNTIME) // it will be available for java compiler and jvm 
@Target(ElementType.METHOD)// used to specify where we can use the annotation 
public @interface MyAnnotation {

	int value();
}
