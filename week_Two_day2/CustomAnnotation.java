package com.org.coll2;

import java.lang.annotation.Retention;
import java.lang.reflect.Method;

//diver class which is using my custom annotation 

public class CustomAnnotation {
	//using annotaion
	@TestAnnotation(Tester =" Mohit ",TestCase =   "Lgoin testcase ")
	void QAInfo() {
		System.out.println(" This is QA method");
	}
	
	@TestAnnotation(Tester = "Rohit",TestCase = "Account Check")
	void QCInfo() {
		System.out.println(" This is QC method");
	}

	public static void main(String[] args) throws NoSuchMethodException, SecurityException {
		// TODO Auto-generated method stub
System.out.println(" Checking custom annotation ");
CustomAnnotation c1= new CustomAnnotation(); // class object 

c1.QAInfo();c1.QCInfo();
Method m= c1.getClass().getMethod("QCInfo");

TestAnnotation test = m.getAnnotation(TestAnnotation.class);
System.out.println(" Value is  : "+ test.TestCase());

	}

}
