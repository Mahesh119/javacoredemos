package com.org.coll2;

import java.util.LinkedList;
import java.util.Queue;
// question - create linkedlist with Dqueue and add , remove elements in the program. 
//java linklist as queue
public class ConvertIntoQueue {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
     Queue<String> lang = new LinkedList<>();
     lang.add("Python");lang.add("Java");lang.add("C");lang.add("Kotlin");
     
     //access elements 
     String str = lang.peek();//to access first(head) element
     System.out.println(" Head : "+ str);
   String str1 = lang.poll() ;// removing the first element
		   System.out.println("Head remove : "+ str1);
   lang.offer("Go language ");
   System.out.println(" after using offer : "+ lang);
	}

}
