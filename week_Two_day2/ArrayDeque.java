package com.org.coll2;

import java.util.Deque;

public class ArrayDeque {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    Deque<Integer> num =  new java.util.ArrayDeque<Integer>();
    
    //add elements 
    num.add(10);num.add(20);num.add(30);num.add(50);num.add(24);// return exception if dequeu is full
    
    num.offer(200);num.offer(400);num.offer(500);// return false if the dequeue is full
    System.out.println(" all elements "+ num );
    //access element
    
    int x = num.getFirst();
    System.out.println(" First element "+ x);
    int y = num.getLast();
    System.out.println(" last element "+ y);

    //peek peekfirst  peeklast
    
    System.out.println(" Peek "+ num.peek());
    System.out.println("peek first"+ num.peekFirst());
    System.out.println("peeklast "+num.peekLast());

	//remove elements 
    
    System.out.println(" remove first "+ num.removeFirst());
    System.out.println(" remove Head element "+ num.remove());
    System.out.println(" remove last element "+ num.removeLast());
    for(Integer n : num) {
    	System.out.println(num);
    }
	
	}
	
}
