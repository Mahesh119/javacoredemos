package com.org.coll2;

public class CustomerData {
String name ; 
int id ;
public CustomerData(String name, int id) {
	super();
	this.name = name;
	this.id = id;
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + id;
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	// checking if both obj references are referring to the same object 
	if (this == obj)
		return true;
	//it check if the argument is of the type customerdata  by comparing the classes of the passes arument and 
	//this object if(! obj instanceof Customerdata))return false ---> avoid 
	
	if (obj == null || obj.getClass()!=this.getClass())
	return false;
	CustomerData other = (CustomerData) obj;
	return (other.name ==this.name && other.id==this.id);
	
	
}


}
