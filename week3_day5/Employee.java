package com.org.jdk8_2;

public class Employee {
private int EmpId;
private String name;
private int salary;
public Employee(int empId, String name, int salary) {
	super();
	EmpId = empId;
	this.name = name;
	this.salary = salary;
}
public int getEmpId() {
	return EmpId;
}
public void setEmpId(int empId) {
	EmpId = empId;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getSalary() {
	return salary;
}
public void setSalary(int salary) {
	this.salary = salary;
}
@Override
public String toString() {
	return "Employee [EmpId=" + EmpId + ", name=" + name + ", salary=" + salary + "]";
}

}
