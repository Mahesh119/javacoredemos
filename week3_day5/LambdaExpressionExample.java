package com.org.jdk8_2;
interface ExampleInterface{
	int multi(int a , int b , int c , int d, int p, int d1);
}


public class LambdaExpressionExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
ExampleInterface obj =(a,b,c,d , p ,d1)->((a*a)+(b*c)+(c*a)+(p+d1));

System.out.println(" Result = "+obj.multi(2, 3, 4, 5, 3, 2));//without return keyword


ExampleInterface obj1 = (int a, int b,int c, int d, int p, int d1)->{
	int x= ((a+b)*(b+c)*(p+d1));
	return x;
};
System.out.println(" result 2 = "+ obj1.multi(2, 4, 4, 6, 2, 3));
	}

}
