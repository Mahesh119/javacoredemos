package com.org.jdk8_2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamDemo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
ArrayList<Person> per = new ArrayList<Person>();
per.add(new Person("Jack", 23));
per.add(new Person("Jill", 22));
per.add(new Person("Marry", 24));
per.add(new Person("Jhon", 25));
per.add(new Person("Jack", 21));
per.add(new Person("Adi", 20));
per.add(new Person("Boman", 24));
per.add(new Person("Zara", 21));

Person p1 =per.stream().filter(name-> "Jack" .equals(name.getName())).findAny().orElse(null);
System.out.println(p1);


//multiple condition
Person p2 = per.stream().filter((pname)->"Jack".equals(pname.getName())&& 21== pname.getAge()).findAny().orElse(null);
System.out.println(p2);


int age = per.stream().
filter(a->23<a.getAge())
.map(Person::getAge)
.findAny()
.orElse(null);
System.out.println("Age : "+age);


ArrayList<String> PersonName = (ArrayList<String>) per.stream().map(Person::getName).collect(Collectors.toList());
PersonName.forEach(System.out::println);

//sorting name
/*List<User> sortedList = users.stream()
.sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
.collect(Collectors.toList());*/

List<Person> n1 = per.stream().sorted(Comparator.comparing(Person::getName)).collect(Collectors.toList());
System.out.println(n1);

//search all those name whch starts with 'J'

//boolean matchdata = per.stream().anyMatch((m)->m.startsWith("J"));

	}

}
