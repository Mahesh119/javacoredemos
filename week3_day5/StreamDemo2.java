package com.org.jdk8_2;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Stream;

public class StreamDemo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//stream.of()
		Stream<Integer> st = Stream.of(10,20,30,40,50,60,70);
		st.forEach(p->System.out.println(p));
		
//stream.of()Array
		Stream<Integer> st1 = Stream.of(new Integer[] {12,13,14,15,16,17});
		st1.forEach(p1->System.out.println(p1));
		
//stream creating from List 
		
		ArrayList<String> list = new ArrayList<String>();
		list.add("Delhi");list.add("mumbai");list.add("Nagpur");list.add("Pune");
		System.out.println("list collection data : "+list);
		Stream<String> st2 = list.stream();//adding collection data into stream
		st2.forEach(p2->System.out.println("Stream data :"+p2));
		
//creating stream form generate() or iterate () 
		
		Stream<Integer> st3 = Stream.generate(()->(new Random()).nextInt(100));
		
		st3.limit(20).forEach(System.out::println);//want to generate 20 random numbers 
	}

}
