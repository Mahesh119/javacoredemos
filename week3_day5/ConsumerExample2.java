package com.org.jdk8_2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
/*explanation 
 * ConsumerFunctionExample is my class with 2 methods � main() & printList().
In main() method first an instance of Consumer<Integer> is defined(named consumer) using a 
lambda expression which takes input as an object of type 
Integer and contains logic to print the value of that Integer.
Next the main() method defines a new list of integers and passes it to the printList() method 
along with the consumer object defined earlier which contains the printing logic.
The printList() method iterates through the list of integers and invokes the accept() 
method of the consumer object for every integer in the list.
The accept() method which works as per the lambda definition assigned to the consumer interface, i.e. i-> System.out.print(" "+i) , 
prints out the value of each integer with a single space character prepended to it. 
Thus giving the output */
public class ConsumerExample2 {
public static void printList(List<Integer> l1 , Consumer<Integer> con) {
	for(Integer it : l1) {
		con.accept(it);
	}
}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Consumer<Integer> con = i->System.out.println("   "+ i);
	  List<Integer> list= Arrays.asList(new Integer(12),new Integer(20),new Integer(50));
	  printList(list, con);
	  
	  
	}

}
