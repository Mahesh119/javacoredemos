package com.org.jdk8_2;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamCollectorsDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
   
   
   ArrayList<Integer> arr1 = new ArrayList<Integer>();
   for(int i=1 ; i<=20 ;i++) {
	   arr1.add(i);
	   
	   Stream<Integer> s1 = arr1.stream();
	   ArrayList<Integer> even = (ArrayList<Integer>) s1.filter(x->x%2==0).collect(Collectors.toList());
	   System.out.println(" Even Numbers : "+even);
   }
   
	}

}
