package com.org.jdk8_2;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample3 {
	public static void printList(List<Integer> l1 , Consumer<Integer> con) {
		for(Integer it : l1) {
			con.accept(it);}
		}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Consumer<Integer> con = i->System.out.println("   "+ i);
		Consumer<Integer> con2=con.andThen(i->System.out.println("(printed "+i+")"));
		  List<Integer> list= Arrays.asList(new Integer(12),new Integer(20),new Integer(50));
		  printList(list, con2);
		}

}
