package com.org.jdk8_2;
/*parallel processing is all about dividing a larger 
 * task into smaller sub tasks (forking), then processing the sub tasks in parallel 
 * and then combining the results together to get the final output (joining). Java 8 Streams API provides a similar 
 * mechanism to work with Java Collections. The Java 8 Streams concept is based on converting Collections to a Stream,
 *  processing the elements in parallel and then gathering the resulting elements into a Collection.
 *  
 *  
 *Collections are in-memory data structures which hold elements within it. Each element in the collection is computed before 
 *it actually
 * becomes a part of that collection. On the other hand Streams are fixed data structures which computes the elements on-demand
 *  basis.  */
public class StreamAPIDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
