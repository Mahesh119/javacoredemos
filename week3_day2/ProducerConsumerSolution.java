package com.org.multiT2;

import java.util.Vector;

class Producer implements  Runnable {
private final Vector sharedQueue;
private final int size;

	public Producer(Vector sharedQueue, int size) {
	super();
	this.sharedQueue = sharedQueue;
	this.size = size;
}

	@Override
	public void run() {
		// TODO Auto-generated method stub
	for(int i=0;i<7;i++) {
		System.out.println("Produced : "+i);
		try {
			produce(i);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	}
	private void produce(int i ) throws InterruptedException {
		while(sharedQueue.size()==size) {
			synchronized (sharedQueue) {
				System.out.println(" The Queue is Full   "+ Thread.currentThread().getName()+" is waiting , size "+ sharedQueue.size());
				sharedQueue.wait();
			}
		}
		synchronized (sharedQueue) {
			sharedQueue.add(i);
			sharedQueue.notifyAll();
		}
	}

}
class Consumer implements Runnable{
private final Vector sharedQueue;
private final int size;

	public Consumer(Vector sharedQueue, int size) {
	super();
	this.sharedQueue = sharedQueue;
	this.size = size;
}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true) {
			try {
				System.out.println("Consumed : "+ consume());
				Thread.sleep(500);
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	private int consume() throws InterruptedException {
		while(sharedQueue.isEmpty()) {
			synchronized (sharedQueue) {
				System.out.println("The queue is empty "+ Thread.currentThread().getName()+" is waiting , size"+sharedQueue.size());
			sharedQueue.wait();
			}
		}synchronized (sharedQueue) {
			sharedQueue.notifyAll();
			return (Integer)sharedQueue.remove(0);
		}
	}
	
	
}
public class ProducerConsumerSolution {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Vector sharedQueue = new Vector ();
int size = 4;
Thread  pro = new Thread(new Producer(sharedQueue, size), "Producer ");
Thread con = new Thread(new Consumer(sharedQueue, size),"Consumer");
pro.start();
con.start();
	}

}
