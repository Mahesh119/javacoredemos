package com.org.multiT2;
class Employee{
	int salary = 50000;
	
synchronized void withdraw(int salary) {
		System.out.println(" Ready to withdraw .....");
		if(this.salary <salary) {
			System.out.println(" Less balance ...can not withdraw");
			try {
				wait();
			}catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}
		}
		this.salary -=salary;
		System.out.println(" withdraw Done !!!!!!");
		
	}
	
synchronized void deposite(int salary) {
		System.out.println(" Depositing the salary .....");
		this.salary+=salary;
		System.out.println("Deposit Done ....");
		notify();
		
	}
}//thread1 --run--> withdraw    thread2---run-->deposit
public class HCL_Employee {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
     Employee e1= new Employee();
     new Thread() {
    	 public void run() {
    		 e1.withdraw(6000);
    	 }
     }.start();
     new Thread() {
    	 public void run() {
    		 e1.deposite(1000);
    	 }
     }.start();
	}

}
