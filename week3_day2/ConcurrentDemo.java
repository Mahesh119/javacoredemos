package com.org.multiT2;

import java.util.ArrayList;
import java.util.Iterator;
//The main problem is when one thread is iterating an Collections object then if another thread cant modify 
//the content of the object. If another 
//thread try to modify the content of object then we will get RuntimeException saying ConcurrentModificationException.
public class ConcurrentDemo extends Thread {
static ArrayList<String> l = new ArrayList<String>();

public void run() {
	try {
		Thread.sleep(2000);
	}catch (InterruptedException e) {
		// TODO: handle exception
		System.out.println(" Child thread going to add element ");
	}
	//child thread trying to add new element in the collection  object 
	l.add("xyz"); // here try to modify the collection with another child object 
}
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
     
		l.add("A");l.add("B");l.add("C");
		
		//we create child thread that is going to modify arraylist
		ConcurrentDemo  t =  new ConcurrentDemo();
		t.start();
		Iterator it = l.iterator();
		while(it.hasNext()) {
			String s = (String) it.next();
			System.out.println(s);
			Thread.sleep(5000);
		}
     
    	System.out.println(l);
     
	}

}
